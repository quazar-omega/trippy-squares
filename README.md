# Trippy squares

A trippy square spinner that I could watch for hours,  
you can also tweak its spinner count!  

Try it [here](https://quazar-omega.gitlab.io/trippy-squares)!
