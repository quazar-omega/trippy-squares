let container = document.getElementById("container");

let quantity = document.getElementById("quantity");
let quantityDisplay = document.getElementById("quantity-display");

let duration = document.getElementById("duration");
let durationDisplay = document.getElementById("duration-display");

let hue = document.getElementById("hue");
let hueDisplay = document.getElementById("hue-display");

let saturation = document.getElementById("saturation");
let saturationDisplay = document.getElementById("saturation-display");

let lightness = document.getElementById("lightness");
let lightnessDisplay = document.getElementById("lightness-display");

function updateSquares() {
	container.innerHTML = "";
	let parentSquare = container;
	for (let i = 0; i < quantity.value; i++) {
		let currentSquare = document.createElement("div");
		currentSquare.classList.add("square");
	
		currentSquare.style.height = `${200 - (i * 18)}px`;
		currentSquare.style.width = `${200 - (i * 18)}px`;
		currentSquare.style.backgroundColor = `hsl(
			${hue.value}, 
			${saturation.value}%, 
			${lightness.value * (i / quantity.value + 0.6)}%
		)`;
		currentSquare.style.animationDuration = `${duration.value}s`;
		parentSquare.append(currentSquare);
		parentSquare = currentSquare;
	}
}

updateSquares();

quantity.addEventListener("input", (event) => {
	quantityDisplay.innerText = event.target.value;
	updateSquares();
});

duration.addEventListener("input", (event) => {
	durationDisplay.innerText = `${event.target.value}s`;
	updateSquares();
});

hue.addEventListener("input", (event) => {
	hueDisplay.innerText = `${event.target.value}°`;
	updateSquares();
});

saturation.addEventListener("input", (event) => {
	saturationDisplay.innerText = `${event.target.value}%`;
	updateSquares();
});

lightness.addEventListener("input", (event) => {
	lightnessDisplay.innerText = `${event.target.value}%`;
	updateSquares();
});